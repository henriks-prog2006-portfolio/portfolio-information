# Portfolio prog2006 - Advanced Programming spring 2024
Author: Henrik Guthormsen

This README documents my portfolio structure for prog2006 with links to all the relevant projects. It also contains a link to the show-and-tell video. 
The portfolio projects are all stored and organized in the "Henriks Prog2006 portfolio" group on my personal GitLab due to NTNU's gitlab having issues changing project settings.  


#### *Link to entire portfolio group:*

[Portfolio](https://gitlab.com/henriks-prog2006-portfolio)

## Portfolio Contents/Projects

### Haskell projects
- Lab01 -  [Link Lab01](https://gitlab.com/henriks-prog2006-portfolio/lab1)
- Lab02 -  [Link Lab02](https://gitlab.com/henriks-prog2006-portfolio/lab02)
- Lab03 -  [Link Lab03](https://gitlab.com/henriks-prog2006-portfolio/lab03)
- Lab04 -  [Link Lab04](https://gitlab.com/henriks-prog2006-portfolio/lab04)

### Rust Projects
- Lab07 -  [Link Lab07](https://gitlab.com/henriks-prog2006-portfolio/lab07)
- Lab09 -  [Link Lab09](https://gitlab.com/henriks-prog2006-portfolio/lab09)
- Lab10 -  [Link Lab10](https://gitlab.com/henriks-prog2006-portfolio/lab10)
- Lab11 -  [Link Lab11](https://gitlab.com/henriks-prog2006-portfolio/lab11)
- Generator Task - [Link Generator Task](https://gitlab.com/henriks-prog2006-portfolio/generator-task)


## Demo Video
- [Portfolio Demo on YouTube](https://www.youtube.com/watch?v=x8UGOe5SIKg)

